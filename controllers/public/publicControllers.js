const { getALL } = require('../../db/conexion');
require('dotenv').config({ path: '.env' });
const BASE_URL = "http://localhost:3000";

const publicControllers = {
  index: async function (req, res) {
    try {
      const integrantes = await getALL("SELECT * FROM Integrante WHERE esta_borrado = FALSE");
      const matriculaDeHome = "Home";
      const Home = await getALL(`SELECT * FROM media WHERE matricula = '${matriculaDeHome}'`);
      res.render("index", {
        data: Home,
        BASE_URL,
        integrantes: integrantes,
        ENLACE: process.env.ENLACE,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
      });
    } catch (error) {
      console.error(error);
      res.status(500).send("Error interno del servidor");
    }
  },

  integranteDetails: async function (req, res) {
    try {
      const matricula = req.params.matricula;
      const integrantes = await getALL(`SELECT * FROM Integrante WHERE esta_borrado = FALSE`);
      const datos = await getALL(`SELECT * FROM media WHERE matricula = '${matricula}'`);
      if (integrantes.length === 0) {
        res.status(404).render("error");
      } else {
        res.render("integrante", {
          data: datos,
          BASE_URL,
          integrantes: integrantes
        });
      }
    } catch (error) {
      console.error(error);
      res.status(500).send("Error interno del servidor");
    }
  },

  wordCloud: async function (req, res) {
    try {
      const integrantes = await getALL("SELECT * FROM integrante");
      res.render("word_cloud", { integrantes: integrantes });
    } catch (error) {
      console.error(error);
      res.status(500).send("Error interno del servidor");
    }
  },

  curso: async function (req, res) {
    try {
      const integrantes = await getALL("SELECT * FROM integrante");
      res.render("curso", { integrantes: integrantes });
    } catch (error) {
      console.error(error);
      res.status(500).send("Error interno del servidor");
    }
  }
};

module.exports = publicControllers;
