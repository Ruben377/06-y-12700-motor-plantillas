const bcrypt = require('bcrypt');
const { db, getALL } = require('../../db/conexion');

// Autenticación de administrador para el registro de nuevos usuarios
const adminAuthenticate = (req, res) => {
    const { usuario, password } = req.body;

    const query = `SELECT * FROM usuarios WHERE usuario = ? AND admin = 0`;
    db.get(query, [usuario], async (err, user) => {
        if (err) {
            console.error('Error querying the database:', err);
            res.status(500).send('Error during admin authentication');
            return;
        }

        if (!user) {
            res.render('login/adminAuth', { error: 'Credenciales de administrador incorrectas' });
            return;
        }

        try {
            if (await bcrypt.compare(password, user.password)) {
                // Autenticación exitosa, guardar la sesión de autenticación del administrador
                req.session.isAdminAuthenticated = true;
                res.redirect('/auth/register');
            } else {
                res.render('login/adminAuth', { error: 'Credenciales de administrador incorrectas' });
            }
        } catch (error) {
            console.error('Error comparing passwords:', error);
            res.status(500).send('Error during admin authentication');
        }
    });
};

// Registro de usuario
const register = async (req, res) => {
    if (!req.session.isAdminAuthenticated) {
        res.redirect('/auth/admin-auth');
        return;
    }

    const { usuario, password, sysadmin = false, admin = false } = req.body;

    const queryCheckUser = `SELECT * FROM usuarios WHERE usuario = ?`;

    db.get(queryCheckUser, [usuario], async (err, existingUser) => {
        if (err) {
            console.error('Error querying the database:', err);
            res.status(500).send('Error registering user');
            return;
        }

        if (existingUser) {
            res.render('login/register', { error: 'El nombre de usuario ya existe' });
            return;
        }

        try {
            const hashedPassword = await bcrypt.hash(password, 10);
            const query = `INSERT INTO usuarios (usuario, password, sysadmin, admin) VALUES (?, ?, ?, ?)`;
            db.run(query, [usuario, hashedPassword, sysadmin ? 1 : 0, admin ? 1 : 0], function(err) {
                if (err) {
                    console.error('Error registering user:', err);
                    res.status(500).send('Error registering user');
                } else {
                    req.session.isAdminAuthenticated = false; // Limpiar la autenticación de admin
                    res.render('login/login', { success: 'Usuario registrado con éxito. Por favor, inicie sesión.' });
                }
            });
        } catch (error) {
            console.error('Error hashing password:', error);
            res.status(500).send('Error registering user');
        }
    });
};

// Login de usuario
const login = (req, res) => {
    const { usuario, password } = req.body;

    const query = `SELECT * FROM usuarios WHERE usuario = ?`;
    db.get(query, [usuario], async (err, user) => {
        if (err) {
            console.error('Error querying the database:', err);
            res.status(500).send('Error logging in');
            return;
        }

        if (!user) {
            res.render('login/login', { error: 'Usuario o contraseña incorrectos' });
            return;
        }

        try {
            if (await bcrypt.compare(password, user.password)) {
                req.session.logueado = {
                    id: user.id,
                    usuario: user.usuario,
                    sysadmin: user.sysadmin,
                    admin: user.admin
                };
                res.redirect('/admin');
            } else {
                res.render('login/login', { error: 'Usuario o contraseña incorrectos' });
            }
        } catch (error) {
            console.error('Error comparing passwords:', error);
            res.status(500).send('Error logging in');
        }
    });
};

const logout = (req, res) => {
    // Destruir la sesión del usuario
    req.session.destroy((err) => {
        if (err) {
            console.error('Error al cerrar sesión:', err);
            res.status(500).send('Error al cerrar sesión');
        } else {
            // Redirigir al usuario al inicio de sesión
            res.redirect('/auth/login');
        }
    });
};

// Destruir sesión del administrador
const adminLogout = (req, res) => {
    req.session.isAdminAuthenticated = false;
    res.redirect('/auth/admin-auth');
};

module.exports = { register, login, logout, adminAuthenticate, adminLogout };
