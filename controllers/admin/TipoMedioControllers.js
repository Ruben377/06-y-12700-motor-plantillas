const express = require('express');
const { getALL, db } = require('../../db/conexion'); 
const TipoMedioStoreSchema = require('../../validators/TipoMedio/createTipoMedio');
const TipoMedioEditSchema = require('../../validators/TipoMedio/editTipoMedio')
const TipoMedioModel = require('../../models/TipoMedioModel')


const TipoMedioCotroller = {
    index: async function (req, res) {
        try {
            const filters = req.query.s || {};
            const tipoMedioItems = await TipoMedioModel.getALL(filters);

            const message = req.query.message || null;

            if (tipoMedioItems.length === 0) {
                return res.render("admin/TipoMedio/index", { message: "No hay elementos de tipo de medio", tipoMedioItems: [] });
            }

            res.render("admin/TipoMedio/index", { message, tipoMedioItems });
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },
    create: async function (req, res) {
        const mensaje = req.query.mensaje;
        const datosForm = req.query;
        res.render('admin/TipoMedio/crearForm', {
            mensaje,
            datosForm,
        });
    },

    store: async function (req, res) {
        try {
            // Validar los datos de entrada
            const { error, value } = TipoMedioStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const errorMessage = error.details.map(detail => detail.message).join(', ');
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=${encodeURIComponent(errorMessage)}&${queryParams}`);
            }

            const { tipo, descripcion, esta_borrado } = value;

            const tipoExistente = await new Promise((resolve, reject) => {
                db.get("SELECT COUNT(*) as count FROM TipoMedio WHERE tipo = ?", [tipo], (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row.count > 0);
                });
            });

            if (tipoExistente) {
                const queryParams = new URLSearchParams(req.body).toString();
                return res.redirect(`/admin/TipoMedio/crear?mensaje=El tipo ya existe&${queryParams}`);
            }

            const maxIdResult = await new Promise((resolve, reject) => {
                db.get("SELECT MAX(id) as maxId FROM TipoMedio", (err, row) => {
                    if (err) {
                        return reject(err);
                    }
                    resolve(row);
                });
            });

            const newId = (maxIdResult.maxId || 0) + 1;

            db.run(
                "INSERT INTO TipoMedio (id, tipo, descripcion, esta_borrado) VALUES (?, ?, ?, ?)",
                [newId, tipo, descripcion, esta_borrado],
                (err) => {
                    if (err) {
                        console.error("Error al insertar en la base de datos:", err);
                        return res.redirect("/admin/TipoMedio/crear?mensaje=Error al crear el tipo de medio");
                    }
                    res.redirect("/admin/TipoMedio/listar?message=Tipo de Medio creado exitosamente");
                }
            );
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },

    edit: function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;
    
        db.get("SELECT * FROM TipoMedio WHERE id = ? ", [idTipoMedio], (err, tipoMedio) => {
            if (err) {
                console.error("Error al obtener el tipo de medio:", err);
                return res.status(500).render("error");
            }
    
            if (!tipoMedio) {
                return res.redirect('/admin/TipoMedio/listar?message=No se encontró el tipo de medio');
            }
    
            res.render("admin/TipoMedio/editForm", { tipoMedio });
        });
    },

    update: function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;
    
        // Validar el cuerpo de la solicitud usando Joi
        const { error, value } = TipoMedioEditSchema.validate(req.body, { abortEarly: false });
    
        if (error) {
            const errorMessage = error.details.map(detail => detail.message).join(', ');
            console.error("Error de validación:", errorMessage);
            return res.redirect(`/admin/TipoMedio/edit/${idTipoMedio}?mensaje=${encodeURIComponent(errorMessage)}`);
        }
    
        const { tipo, descripcion, esta_borrado } = value;
    
        db.run(
            "UPDATE TipoMedio SET tipo = ?, descripcion = ?, esta_borrado = ? WHERE id = ?",
            [tipo, descripcion, esta_borrado, idTipoMedio],
            function (err) {
                if (err) {
                    console.error("Error al actualizar el tipo de medio:", err);
                    return res.redirect(`/admin/TipoMedio/edit/${idTipoMedio}?mensaje=Error al actualizar el tipo de medio`);
                }
                console.log(`Tipo de medio con ID ${idTipoMedio} actualizado.`);
                res.redirect('/admin/TipoMedio/listar?message=Tipo de medio actualizado exitosamente');
            }
        );
    },

    destroy: function (req, res) {
        const idTipoMedio = req.params.idTipoMedio;

        db.run("UPDATE TipoMedio SET esta_borrado = 1 WHERE id = ?", [idTipoMedio], function (err) {
            if (err) {
                console.error("Error al eliminar el tipo de medio:", err);
                return res.redirect('/admin/TipoMedio/listar?message=Error al eliminar el tipo de medio');
            }

            console.log(`Tipo de medio con ID ${idTipoMedio} eliminado.`);
            return res.redirect('/admin/TipoMedio/listar?message=Tipo de medio eliminado exitosamente');
        });
    }
};

module.exports = TipoMedioCotroller;
