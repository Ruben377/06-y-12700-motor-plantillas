const express = require('express');
const { db } = require('../../db/conexion'); 
const multer = require('multer');
const upload = multer({ dest: '../../public/assets' });
const fileUpload = upload.single('imagen');
const IntegranteStoreSchema = require('../../validators/Integrante/createIntegrante');
const IntegranteEditSchema = require('../../validators/Integrante/editIntegrante');
const IntegranteModel = require('../../models/IntegranteModel');

const translateError = (error) => {
    switch (error.type) {
        case "string.min":
            return `${error.context.label} debe ser mínimo de ${error.context.limit} caracteres`;
        case "any.required":
            return `${error.context.label} es un campo requerido`;
        case "string.uri":
            return `${error.context.label} debe ser una URL válida`;
        case "any.only":
            return `${error.context.label} debe ser uno de [${error.context.valids.join(', ')}]`;
        case "boolean.base":
            return `${error.context.label} debe ser activo o inactivo`;
        default:
            return error.message;
    }
};

const IntegranteController = {
    index: async function (req, res) {     
        try {
            const filters = req.query.s || {};
            const integrantes = await IntegranteModel.getALL(filters);

            const message = req.query.message || null;

            if (integrantes.length === 0) {
                return res.render("admin/Integrante/index", { message: "No hay integrantes", integrantes: [] });
            }

            res.render("admin/Integrante/index", { integrantes, message ,logueado: req.session.userInfo?.logueado});
        } catch (error) {
            console.error(error);
            return res.status(500).render("error");
        }
    },
    create: async function (req, res) {
        const message = req.query.message;
        const datosForm = req.query;
        res.render('admin/Integrante/crearForm', {
            message: message,
            datosForm: datosForm,
        });
    },
    store: async function (req, res) {
        try {
            const { error, value } = IntegranteStoreSchema.validate(req.body, { abortEarly: false });
            if (error) {
                const errorMessage = error.details.map(detail => translateError(detail)).join(', ');
                return res.render("admin/Integrante/crearForm", {
                    mensaje: errorMessage,
                    datosForm: req.body
                });
            }

            const result = await IntegranteModel.create(value);

            res.redirect("/admin/Integrante/listar?message=Integrante creado exitosamente");
        } catch (error) {
            console.error('Error al insertar en la base de datos:', error);
            res.status(500).send('Error al insertar en la base de datos');
        }
    },
    edit: async function (req, res) {
        try {
            const idIntegrante = req.params.idIntegrante;
            console.log("ID del Integrante:", idIntegrante);

            const integrante = await IntegranteModel.getById(idIntegrante);

            if (!integrante) {
                return res.redirect('/admin/Integrante/listar?message=No se encontró el integrante');
            }

            res.render("admin/Integrante/editForm", { integrante });
        } catch (error) {
            console.error("Error al obtener el integrante:", error);
            return res.status(500).render("error");
        }
    },
    update: async function (req, res) {
        try {
            const idIntegrante = req.params.idIntegrante;
            const { error, value } = IntegranteEditSchema.validate(req.body, { abortEarly: false });

            if (error) {
                const errorMessage = error.details.map(detail => translateError(detail)).join(', ');
                console.error("Error de validación:", errorMessage);
                return res.redirect(`/admin/Integrante/edit/${idIntegrante}?message=${encodeURIComponent(errorMessage)}`);
            }

            const result = await IntegranteModel.update(idIntegrante, value);

            res.redirect('/admin/Integrante/listar?message=Integrante actualizado exitosamente');
        } catch (error) {
            console.error("Error al actualizar el integrante:", error);
            return res.redirect(`/admin/Integrante/edit/${idIntegrante}?message=Error al actualizar el integrante`);
        }
    },
    destroy: async function (req, res) {
        try {
            const idIntegrante = req.params.idIntegrante;

            const result = await IntegranteModel.delete(idIntegrante);

            res.redirect('/admin/Integrante/listar?message=Integrante eliminado exitosamente');
        } catch (error) {
            console.error("Error al eliminar el integrante:", error);
            return res.redirect('/admin/Integrante/listar?message=Error al eliminar el integrante');
        }
    }
};

module.exports = IntegranteController;
