const express = require('express');
const router = express.Router();
const publicController = require('../controllers/public/publicControllers');
const authController = require('../controllers/login/authControllers');

// Rutas del proyecto
router.get('/auth/login', (req, res) => {
    res.render('login/login');
});

router.post('/login', authController.login);

router.get('/auth/register', (req, res) => {
    if (!req.session.isAdminAuthenticated) {
        res.redirect('/auth/admin-auth');
        return;
    }
    res.render('login/register');
});

router.post('/register', authController.register);

// Ruta para autenticación de administrador
router.get('/auth/admin-auth', (req, res) => {
    res.render('login/adminAuth');
});

router.post('/auth/admin-authenticate', authController.adminAuthenticate);

// Ruta para logout de administrador
router.get('/auth/admin-logout', authController.adminLogout);

// Ruta para logout
router.get('/auth/logout', authController.logout);

router.get('/', publicController.index);

router.get('/integrantes/:matricula', publicController.integranteDetails);

router.get('/paginas/word_cloud.html', publicController.wordCloud);

router.get('/paginas/curso.html', publicController.curso);

module.exports = router;
