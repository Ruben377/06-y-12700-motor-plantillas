const Joi = require('joi');

const TipoMedioStoreSchema = Joi.object({
    tipo: Joi.string().min(3).max(50).required().messages({
        'string.empty': 'El tipo es un campo obligatorio',
        'string.min': 'El tipo debe tener al menos 3 caracteres',
        'string.max': 'El tipo debe tener como máximo 50 caracteres'
    }),
    descripcion: Joi.string().min(5).max(255).required().messages({
        'string.empty': 'La descripción es un campo obligatorio',
        'string.min': 'La descripción debe tener al menos 5 caracteres',
        'string.max': 'La descripción debe tener como máximo 255 caracteres'
    }),
    esta_borrado: Joi.number().integer().valid(0, 1).required().messages({
        'any.required': 'El estado es un campo obligatorio',
        'number.base': 'El estado debe ser un número entero',
        'any.only': 'El estado debe ser 0 (Activo) o 1 (Inactivo)'
    })
}).options({ allowUnknown: true });

module.exports = TipoMedioStoreSchema;
