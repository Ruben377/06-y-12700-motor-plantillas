const sqlite3 = require("sqlite3").verbose();

const db = new sqlite3.Database(
  "./db/integrantes.sqlite",
  sqlite3.OPEN_READWRITE,
  (error) => {
    if (error) console.log("Ocurrió un error", error.message);
    else {
      console.log("Conexión Exitosa");
    }
  }
);

async function getALL(query, params) {
  return new Promise((resolve, reject) => {
    db.all(query, params, (error, rows) => {
      if (error) return reject(error); 
      resolve(rows);
    });
  });
}

module.exports = { db, getALL };
