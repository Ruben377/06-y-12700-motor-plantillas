-- Crear tabla Integrante
CREATE TABLE IF NOT EXISTS Integrante (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    matricula VARCHAR(255) NOT NULL UNIQUE,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    rol VARCHAR(255) NOT NULL,
    codigo VARCHAR(255) NOT NULL,
    url VARCHAR(255) NOT NULL,
    esta_borrado BOOLEAN DEFAULT FALSE,
    orden INTEGER DEFAULT 0
);

-- Crear tabla TipoMedio
CREATE TABLE IF NOT EXISTS TipoMedio (
    tipo VARCHAR(255) NOT NULL PRIMARY KEY,
    descripcion TEXT NOT NULL,
    esta_borrado BOOLEAN DEFAULT FALSE,
    orden INTEGER DEFAULT 0
);

-- Crear tabla Media
CREATE TABLE IF NOT EXISTS Media (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    matricula VARCHAR(255) NOT NULL,
    url VARCHAR(255) NOT NULL,
    titulo VARCHAR(255) NOT NULL,
    parrafo TEXT NOT NULL,
    imagen TEXT,
    video TEXT,
    html TEXT,
    tipo_medio VARCHAR(255) NOT NULL,
    esta_borrado BOOLEAN DEFAULT FALSE,
    orden INTEGER DEFAULT 0,
    FOREIGN KEY (matricula) REFERENCES Integrante(matricula),
    FOREIGN KEY (tipo_medio) REFERENCES TipoMedio(tipo)
);

-- Crear índices para la tabla Media
CREATE INDEX IF NOT EXISTS idx_media_tipo ON Media(tipo_medio);
CREATE INDEX IF NOT EXISTS idx_media_integrante ON Media(matricula);

-- Crear tabla usuarios
CREATE TABLE IF NOT EXISTS usuarios (
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    usuario TEXT NOT NULL UNIQUE,
    password TEXT NOT NULL,
    sysadmin BOOLEAN DEFAULT 0,
    admin BOOLEAN DEFAULT 0,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Insertar usuario admin
INSERT OR IGNORE INTO usuarios (usuario, password, sysadmin, admin) VALUES ('admin', '$2b$10$wHcBfA1fZ1R2JstltQj.DOE0GpNZ2u/FcxWY8dF.4iKZLgY4.NBva', 0, 1);

-- Insertar usuario Ruben
INSERT OR IGNORE INTO usuarios (usuario, password, sysadmin, admin) VALUES ('Ruben37', 'olimperoru37', 0, 0);


--$2b$10$N9qo8uLOickgx2ZMRZo5i.U5vG7xoxJcTofHcGqXfNSB2yEvlCoOm//