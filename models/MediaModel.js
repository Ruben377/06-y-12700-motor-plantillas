const { getALL, db } = require('../db/conexion'); 

const MediaModel = {
    getALL: async function (filters) {
        return new Promise((resolve, reject) => {
            let sql = `
                SELECT Media.*, Integrante.nombre AS nombre_integrante
                FROM Media
                INNER JOIN Integrante ON Media.matricula = Integrante.matricula
                WHERE Media.esta_borrado = 0
            `;

            const params = [];

            // Definir columnas válidas para filtrar
            const validColumns = ['id', 'matricula', 'titulo', 'tipo_medio', 'orden'];

            if (filters) {
                for (const [key, value] of Object.entries(filters)) {
                    if (validColumns.includes(key) && value) {
                        sql += ` AND Media.${key} = ?`;
                        params.push(value);
                    }
                }
            }

            db.all(sql, params, (err, rows) => {
                if (err) {
                    return reject(err);
                }
                resolve(rows);
            });
        });
    },
    getById: async function (id) {
        try {
            const sql = `
                SELECT Media.*, Integrante.nombre AS nombre_integrante
                FROM Media
                INNER JOIN Integrante ON Media.matricula = Integrante.matricula
                WHERE Media.id = ? AND Media.esta_borrado = 0
            `;
            const mediaItems = await getALL(sql, [id]);

            if (mediaItems.length === 0) {
                return null;
            }

            const processedMediaItem = {
                ...mediaItems[0],
                esta_borrado: mediaItems[0].esta_borrado ? 'Inactivo' : 'Activo'
            };

            return processedMediaItem;
        } catch (error) {
            console.error(error);
            throw new Error("Error al obtener el elemento de media");
        }
    },

    getByField: async function (field, value) {
        try {
            const sql = `
                SELECT Media.*, Integrante.nombre AS nombre_integrante
                FROM Media
                INNER JOIN Integrante ON Media.matricula = Integrante.matricula
                WHERE Media.${field} = ? AND Media.esta_borrado = 0
            `;
            const mediaItems = await getALL(sql, [value]);

            if (mediaItems.length === 0) {
                return null;
            }

            const processedMediaItems = mediaItems.map(item => ({
                ...item,
                esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
            }));

            return processedMediaItems;
        } catch (error) {
            console.error(error);
            throw new Error("Error al obtener el elemento de media");
        }
    },

    create: async function (data) {
        try {
            const { matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden } = data;
            const sql = `
                INSERT INTO Media (matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
            `;
            await db.run(sql, [matricula, url, titulo, parrafo, imagen, video, tipo_medio, esta_borrado, orden]);
            return { success: true };
        } catch (error) {
            console.error(error);
            throw new Error("Error al crear el elemento de media");
        }
    },

    update: async function (id, data) {
        try {
            const { titulo, parrafo, imagen, video, esta_borrado } = data;
            const sql = `
                UPDATE Media
                SET titulo = ?, parrafo = ?, imagen = ?, video = ?, esta_borrado = ?
                WHERE id = ?
            `;
            await db.run(sql, [titulo, parrafo, imagen, video, esta_borrado, id]);
            return { success: true };
        } catch (error) {
            console.error(error);
            throw new Error("Error al actualizar el elemento de media");
        }
    },

    delete: async function (id) {
        try {
            const sql = `UPDATE Media SET esta_borrado = 1 WHERE id = ?`;
            await db.run(sql, [id]);
            return { success: true };
        } catch (error) {
            console.error(error);
            throw new Error("Error al eliminar el elemento de media");
        }
    }
};

module.exports = MediaModel;
