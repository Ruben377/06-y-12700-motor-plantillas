const { getALL, db } = require('../db/conexion'); 

const TipoMedioModel = {
    getALL: async function (filters = {}) {
        console.log(filters)
        try {
            let sql = "SELECT * FROM TipoMedio WHERE esta_borrado = 0";
            const params = [];

            for (const prop in filters) {
                if (filters[prop]) {
                    
                    if(prop === 'id') {
                        sql += ` AND ${prop} = ?`;
                        params.push(`${parseInt(filters[prop])}`);
                    } else {
                        sql += ` AND ${prop} LIKE ?`;
                        params.push(`%${filters[prop]}%`);
                    }
                    
                }
            }

            console.log(sql,params)

            const tipoMedioItems = await getALL(sql, params);

            console.log('tipoMedioItems',tipoMedioItems)

            if (tipoMedioItems.length === 0) {
                return [];
            }

            const processedItems = tipoMedioItems.map(item => ({
                ...item,
                esta_borrado: item.esta_borrado ? 'Inactivo' : 'Activo'
            }));

            return processedItems;
        } catch (error) {
            console.error(error);
            throw new Error("Error al obtener los elementos de tipo de medio");
            
        }
        console.log("error", error)
    },
    getById: async function (id) {
        try {
            const sql = "SELECT * FROM TipoMedio WHERE id = ? AND esta_borrado = 0";
            const item = await getALL(sql, [id]);
            return item.length > 0 ? item[0] : null;
        } catch (error) {
            console.error(error);
            throw new Error("Error al obtener el elemento de tipo de medio por ID");
        }
    },

    getByField: async function (field, value) {
        try {
            const sql = `SELECT * FROM TipoMedio WHERE ${field} = ? AND esta_borrado = 0`;
            const item = await getALL(sql, [value]);
            return item.length > 0 ? item[0] : null;
        } catch (error) {
            console.error(error);
            throw new Error(`Error al obtener el elemento de tipo de medio por ${field}`);
        }
    },

    create: async function (data) {
        try {
            const { tipo, descripcion, esta_borrado } = data;
            const maxIdResult = await getALL("SELECT MAX(id) as maxId FROM TipoMedio");
            const newId = (maxIdResult[0].maxId || 0) + 1;

            const sql = "INSERT INTO TipoMedio (id, tipo, descripcion, esta_borrado) VALUES (?, ?, ?, ?)";
            await getALL(sql, [newId, tipo, descripcion, esta_borrado]);
        } catch (error) {
            console.error(error);
            throw new Error("Error al crear el elemento de tipo de medio");
        }
    },

    update: async function (id, data) {
        try {
            const { tipo, descripcion, esta_borrado } = data;
            const sql = "UPDATE TipoMedio SET tipo = ?, descripcion = ?, esta_borrado = ? WHERE id = ?";
            await getALL(sql, [tipo, descripcion, esta_borrado, id]);
        } catch (error) {
            console.error(error);
            throw new Error("Error al actualizar el elemento de tipo de medio");
        }
    },

    delete: async function (id) {
        try {
            const sql = "UPDATE TipoMedio SET esta_borrado = 1 WHERE id = ?";
            await getALL(sql, [id]);
        } catch (error) {
            console.error(error);
            throw new Error("Error al eliminar el elemento de tipo de medio");
        }
    }
}

module.exports = TipoMedioModel;
